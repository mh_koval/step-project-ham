//Our Services

servicesItems.on("click", event => {
    servicesItems.removeClass("services-item-active");
    $(event.target).addClass("services-item-active");
    let index = [...servicesItems].indexOf(event.target);

    servicesShow.removeClass("services-show-active");
    $(servicesShow[index]).addClass("services-show-active");
});

// Our Amazing Work

workMenuLink.on("click", event => {
    event.preventDefault();

    workMenuLink.removeClass("work-link-active");
    $(event.target).addClass("work-link-active");

    clickPoint = $(event.target).attr("data-target");

    clickPoint === "all-items" ? portfolioItem.removeClass("hidden") :
    portfolioItem.addClass("hidden");
    $(`.portfolio-item[data-type="${clickPoint}"]`).removeClass("hidden");
});

workButton.on("click", event => {

        if (hiddenItems.length === 12) $(event.target).remove();

    const firstHiddenItems = $.grep(hiddenItems, (item, index) => {
        if (index < 12) return true;
    });

        $(firstHiddenItems).removeClass("invisible");

        if (clickPoint !== "all-items") {
            $(".portfolio-item").addClass("hidden");
            $(`.portfolio-item[data-type="${clickPoint}"]`).removeClass("hidden");
        }
    })

// Feedback slider

nextArrow.on("click", () => {
    if (slideItemIndex === slideItems.length - 1) return

    slideItemIndex++;
    portraits.removeClass("portrait-active");
    $(portraits[slideItemIndex]).addClass("portrait-active");

    $(".feedback-slide-content").css
    ("transform", `translateX(${-slideItemIndex * slidePosition}px)`);
});

prevArrow.on("click", () => {
    if (slideItemIndex === 0) return;

    slideItemIndex--;
    portraits.removeClass("portrait-active");
    $(portraits[slideItemIndex]).addClass("portrait-active");

    $(".feedback-slide-content").css
    ("transform", `translateX(${-slideItemIndex * slidePosition}px)`);
});


portraits.on("click", event => {
    const portraitsIndex = [...portraits].indexOf(event.target);
    portraits.removeClass("portrait-active");
    $(event.target).addClass("portrait-active");

    $(".feedback-slide-content").css
    ("transform", `translateX(${-portraitsIndex * slidePosition}px)`);
    slideItemIndex = portraitsIndex;
});