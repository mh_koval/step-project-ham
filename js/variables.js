//Our Services
const servicesItems = $(".services-item");
const servicesShow = $(".services-show");

// Our Amazing Work
const workMenuLink = $(".work-menu-link");
const portfolioItem = $(".portfolio-item");
const workButton = $("#work-button");
const hiddenItems = $(".portfolio-item.invisible");
let clickPoint = "all-items";

// Feedback slider
let slideItemIndex = 0;
const slidePosition = $(".feedback-slide-content").width();
const slideItems = $(".slide-item");
const portraits = $(".portrait");
const nextArrow = $("#Next"); 
const prevArrow = $("#Previous");
